@extends('_layouts.master')

@section('content')
    <div class="content">
        <div class="title m-b-md">
            <h1 class="title">About Test</h1>
        </div>

        <div class="links">
            <a href="{{ $base }}/">Home</a>
            <a href="http://jigsaw.tighten.co/">Jigsaw</a>
            <a href="http://jigsaw.tighten.co/docs/installation/">Documentation</a>
            <a href="https://github.com/tightenco/jigsaw">GitHub</a>
        </div>
    </div>
@endsection
